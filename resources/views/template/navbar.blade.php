<div class="container-fluid">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="{{url('/')}}">Fahmi-Algostudio</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item {{ request()->is('/') ? 'active' : ''}}">
          <a class="nav-link" href="{{url('/')}}">Dashboard <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item {{ request()->is('list-penjualan') ? 'active' : ''}}">
          <a class="nav-link" href="{{url('/list-penjualan')}}">List Penjualan</a>
        </li>
      </ul>

    </div>
  </nav>
</div>
