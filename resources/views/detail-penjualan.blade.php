<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
    @include('bootstrap.bootstrapcss')
</head>
<body>
    @include('template.navbar')
    <div class="container">

        <div class="row mt-3">
            <div class="col-12">
                <a href="{{url('list-penjualan')}}" class="btn btn-sm btn-primary mb-3"> <i class="fas fa-chevron-circle-left"></i> Kembali</a>
                <h3>Detail Penjualan</h3>
                <p><b>Nama Konsumen:</b> {{$penjualan->nama_konsumen}}</p>
                <p><b>Alamat:</b> {{$penjualan->alamat}} </p>
                <p><b>Tgl Penjualan:</b> {{$penjualan->tgl_penjualan}} </p>
                <p><b>Daftar Barang Yang Dibeli</b></p>
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Barang</th>
                        <th scope="col">Jumlah</th>
                        <th scope="col">Harga Satuan</th>
                        <th scope="col">Harga Total</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ( $penjualan->detailPenjualan as $index => $p )
                            <tr>
                                <th scope="row">{{$index+1}}</th>
                                <td>{{$p->barang->nama_barang}}</td>
                                <td>{{$p->jumlah}}</td>
                                <td>{{format_uang($p->harga_satuan)}}</td>
                                <td>{{format_uang($p->harga_total)}}</td>
                            </tr>
                        @endforeach

                    </tbody>
                  </table>

            </div>
        </div>
    </div>
    @include('bootstrap.bootstrapjs')

</body>
</html>
