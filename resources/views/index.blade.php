<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
    @include('bootstrap.bootstrapcss')
</head>
<body>
    @include('template.navbar')
    <div class="container">
        <div class="row mt-3">
            <div class="col-6">
                <div>
                    <canvas id="penjualan-chart"></canvas>
                </div>
            </div>
            <div class="col-6">
                <div style="width:60%; margin:auto;">
                    <canvas id="kategori-chart"></canvas>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-12">
                <h4>Penjualan Terakhir</h4>
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Customer</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Tanggal Penjualan</th>
                        <th scope="col">Total Penjualan</th>
                      </tr>
                    </thead>
                    <tbody>

                     @foreach ($penjualan as $index => $p)
                        <tr>
                            <th scope="row">{{$index+1}}</th>
                            <td>{{$p->nama_konsumen}}</td>
                            <td style="max-width:200px;">{{$p->alamat}}</td>
                            <td>{{$p->tgl_penjualan}}</td>
                            <td>{{format_uang($p->detailPenjualan->sum('harga_total'))}}</td>
                        </tr>
                     @endforeach

                    </tbody>
                  </table>

            </div>
        </div>
    </div>
    @include('bootstrap.bootstrapjs')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.0/chart.min.js"></script>
    {{-- penjualan barang chart --}}
    <script>
        //for labels
        let arrOfLabels = @json($penjualanByItem['labels']);
        let filteredLabels = arrOfLabels.filter(function(item, pos){
            return arrOfLabels.indexOf(item)== pos;
        });

        //filter nama barang
        let arrOfNamaBarang = @json($penjualanByItem['nama_barang']);
        let filteredNamaBarang = arrOfNamaBarang.filter(function(item, pos){
            return arrOfNamaBarang.indexOf(item)== pos;
        });


        let objDataSets = [];
        //create object barang based on date of sales
        let barangFinal = @json($penjualanByItem['data_penjualan_barang']);
        let bg = [
                'rgb(255,99,132)',
                'rgb(54,162,235)',
                'rgb(255,205,86)',
                'rgb(153,102,255)',
                'rgb(201,203,207)',
                'rgb(72,61,139)',
                'rgb(148,0,211)',
                'rgb(186,85,211)',
                'rgb(255,105,180)',
                'rgb(139,69,19)',
                'rgb(244,164,96)',
                'rgb(0,191,255)',
                'rgb(0,128,0)'
                ];
        let count = 0;
        filteredNamaBarang.forEach(myFunction);

        function myFunction(value) {

            objDataSets.push({
                label: value,
                data: barangFinal[value],
                backgroundColor: [bg[count], bg[count], bg[count], bg[count], bg[count], bg[count]],
                borderWidth: 1
            });
            count++;

        }

        const dataPenjualan = {
            labels: filteredLabels,
            datasets: objDataSets
        };
        const configPenjualan = {
            type: 'bar',
            data: dataPenjualan,
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            },
        };
        const penjualan = new Chart(
            "penjualan-chart",
            configPenjualan
        );
    </script>
    {{-- persentasi kategori barang chart --}}
    <script>
        const data = {
            labels: @json($penjualanByCategory['labels']),
            datasets: [{
                label: 'Kategori Penjualan',
                data: @json($penjualanByCategory['data']),
                backgroundColor: [
                'rgb(255, 99, 132)',
                'rgb(54, 162, 235)',
                'rgb(255, 205, 86)',
                'rgb(153, 102, 255)',
                'rgb(201, 203, 207)'
                ],
                hoverOffset: 4
            }]
        };

        const config = {
            type: 'pie',
            data: data,
        };

        const kategori = new Chart(
            document.getElementById('kategori-chart'),
            config
        );
    </script>
</body>
</html>
