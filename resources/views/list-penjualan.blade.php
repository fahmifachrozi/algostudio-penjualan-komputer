<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title}}</title>
    @include('bootstrap.bootstrapcss')
</head>
<body>
    @include('template.navbar')
    <div class="container">

        <div class="row mt-3">
            <div class="col-12">
                <table class="table table-striped">
                    <thead>
                      <tr>
                        <th scope="col">No</th>
                        <th scope="col">Nama Customer</th>
                        <th scope="col">Alamat</th>
                        <th scope="col">Tanggal Penjualan</th>
                        <th scope="col">Total Penjualan</th>
                        <th scope="col">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                        @foreach ($penjualan as $index => $p)
                            <tr>
                                <th scope="row">{{($penjualan->currentpage()-1) * $penjualan->perpage() + $index + 1}}</th>
                                <td>{{$p->nama_konsumen}}</td>
                                <td style="max-width:200px;">{{$p->alamat}}</td>
                                <td>{{$p->tgl_penjualan}}</td>
                                <td>{{format_uang($p->detailPenjualan->sum('harga_total'))}}</td>
                                <td><a href="{{url('detail-penjualan/'.$p->id)}}" class='btn btn-md btn-success'>Detail Penjualan</a></td>
                            </tr>
                        @endforeach
                    </tbody>

                  </table>
                  <div class="card-footer py-4" >
                    <nav class="d-flex justify-content-end" aria-label="...">
                        {{$penjualan->links('pagination::bootstrap-4') }}
                    </nav>
                </div>

            </div>

        </div>
    </div>
    @include('bootstrap.bootstrapjs')

</body>
</html>
