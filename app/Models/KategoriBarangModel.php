<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriBarangModel extends Model
{
    use HasFactory;
    protected $table = 'kategori_barang';

    public function barang(){
        return $this->hasMany(BarangModel::class, 'kategori', 'id');
    }

    public function detailPenjualan(){
        return $this->hasManyThrough(DetailPenjualanModel::class, BarangModel::class, 'kategori', 'kode_barang', 'id', 'kode_barang');
    }
}
