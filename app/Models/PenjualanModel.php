<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PenjualanModel extends Model
{
    use HasFactory;
    protected $table = 'penjualan';
    public $timestamps = false;

    public function detailPenjualan(){
        return $this->hasMany(DetailPenjualanModel::class, 'id_penjualan', 'id');
    }


}
