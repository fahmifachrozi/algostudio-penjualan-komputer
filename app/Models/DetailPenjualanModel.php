<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailPenjualanModel extends Model
{
    use HasFactory;

    protected $table = 'detail_penjualan';

    public function penjualan(){
        return $this->belongsTo(PenjualanModel::class, 'id_penjualan', 'id');
    }

    public function barang(){
        return $this->belongsTo(BarangModel::class, 'kode_barang', 'kode_barang');
    }
}
