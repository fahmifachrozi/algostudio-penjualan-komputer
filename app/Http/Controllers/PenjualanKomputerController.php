<?php

namespace App\Http\Controllers;

use App\Models\BarangModel;
use App\Models\DetailPenjualanModel;
use App\Models\KategoriBarangModel;
use App\Models\PenjualanModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PenjualanKomputerController extends Controller
{
    public function dashboard(){
        //data 10 penjualan terakhir
        $getPenjualan = PenjualanModel::latest('tgl_penjualan')->limit(10)->get();

        //data penjualan berdasarkan kategori
        $getPenjualanByCategory = KategoriBarangModel::get();
        $totalPenjualan = DetailPenjualanModel::sum('jumlah');
        $dataPenjualanByCategory = [
            'labels' => [],
            'data' => [],
        ];

        foreach($getPenjualanByCategory as $p){
            $dataPenjualanByCategory['labels'][] = $p->nama_kategori;
            $dataPenjualanByCategory['data'][] = round(($p->detailPenjualan->sum('jumlah') / $totalPenjualan) * 100);
        }

        //data penjualan per hari berdasarkan jenis barang
        $getPenjualanByItem = BarangModel::select('barang.nama_barang', DB::raw('SUM(detail_penjualan.jumlah) as penjualan'), DB::raw('DATE(penjualan.tgl_penjualan) as tgl_penjualan'))
                        ->leftJoin('detail_penjualan', 'detail_penjualan.kode_barang', '=', 'barang.kode_barang')
                        ->leftJoin('penjualan', 'penjualan.id', '=', 'detail_penjualan.id_penjualan')
                        ->where('penjualan.tgl_penjualan', '!=', null)
                        ->orderByRaw('DATE(penjualan.tgl_penjualan)')
                        ->groupByRaw('DATE(penjualan.tgl_penjualan)')
                        ->groupBy('barang.kode_barang')
                        ->get();



        $penjualanByItem = [
            'labels' => [],
            'nama_barang' => [],
            'data_penjualan_barang' => [],
        ];
        $filteredDataBrg = [];
        foreach($getPenjualanByItem as $p){
            //Filter data penjualan barang berdasarkan tgl
            $filteredDataBrg[$p->tgl_penjualan][$p->nama_barang] = $p->penjualan;
            //Filter data label berdasarkan tgl
            $penjualanByItem['labels'][] = $p->tgl_penjualan;
            //Filter nama barang
            $penjualanByItem['nama_barang'][] = $p->nama_barang;
        }

        //Melakukan mapping data penjualan berdasarkan nama barang
        foreach(array_unique($penjualanByItem['nama_barang']) as $brg){
            foreach($filteredDataBrg as $tgl){
                if(isset($tgl[$brg])){
                    $penjualanByItem['data_penjualan_barang'][$brg][] = $tgl[$brg];
                }else{
                    $penjualanByItem['data_penjualan_barang'][$brg][] = 0;
                }
            }

        }

        return view('index', [
            'title' => 'Dashboard',
            'penjualan' => $getPenjualan,
            'penjualanByCategory' => $dataPenjualanByCategory,
            'penjualanByItem' => $penjualanByItem
        ]);
    }

    public function listPenjualan(){
        $getPenjualan = PenjualanModel::orderBy('tgl_penjualan')->paginate(5);

        return view('list-penjualan', ['title' => 'List Penjualan', 'penjualan' => $getPenjualan]);
    }

    public function detailPenjualan($id){
        $getPenjualan = PenjualanModel::where('id', $id)->first();
        return view('detail-penjualan', ['title' => 'Detail Penjualan', 'penjualan' => $getPenjualan]);
    }
}
