<?php

namespace Database\Seeders;

use App\Models\BarangModel;
use App\Models\DetailPenjualanModel;
use App\Models\KategoriBarangModel;
use App\Models\PenjualanModel;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        KategoriBarangModel::factory()->count(3)->create();
        BarangModel::factory()->count(10)->create();
        PenjualanModel::factory()->count(10)->create()->each(function ($penjualan) {
            DetailPenjualanModel::factory()->create(['id_penjualan' => $penjualan->id]);
        });
        DetailPenjualanModel::factory()->count(15)->create();
    }
}
