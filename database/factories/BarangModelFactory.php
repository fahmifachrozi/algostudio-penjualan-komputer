<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BarangModelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $harga = $this->faker->numberBetween(20000, 500000);
        return [
            //
            'kode_barang' => uniqid(),
            'nama_barang' => 'Barang'.uniqid(),
            'harga_jual' => $harga,
            'harga_beli' => $harga-1000,
            'stok' => $this->faker->numberBetween(10, 200),
            'kategori' => $this->faker->numberBetween(1,3)
        ];
    }
}
