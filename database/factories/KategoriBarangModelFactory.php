<?php

namespace Database\Factories;


use Illuminate\Database\Eloquent\Factories\Factory;

class KategoriBarangModelFactory extends Factory
{

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama_kategori' => $this->faker->unique()->randomElement(['processor', 'vga', 'motherboard', 'ram', 'hdd', 'ssd'])
        ];
    }
}
