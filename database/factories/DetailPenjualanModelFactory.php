<?php

namespace Database\Factories;

use App\Models\BarangModel;
use App\Models\PenjualanModel;
use Illuminate\Database\Eloquent\Factories\Factory;

class DetailPenjualanModelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $getPenjualan = PenjualanModel::inRandomOrder()->first();
        $getBarang = BarangModel::inRandomOrder()->first();
        $jumlah = $this->faker->numberBetween(1, 10);
        return [
            //
            'id_penjualan' => $getPenjualan->id,
            'kode_barang' => $getBarang->kode_barang,
            'jumlah' => $jumlah,
            'harga_satuan' => $getBarang->harga_jual,
            'harga_total' => $getBarang->harga_jual * $jumlah
        ];
    }
}
