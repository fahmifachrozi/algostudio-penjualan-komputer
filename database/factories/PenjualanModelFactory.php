<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PenjualanModelFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'tgl_penjualan' => $this->faker->dateTimeBetween('-3 months', '+4 months'),
            'nama_konsumen' => $this->faker->name(),
            'alamat' => $this->faker->address()
        ];
    }
}
