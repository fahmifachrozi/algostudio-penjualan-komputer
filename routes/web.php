<?php

use App\Http\Controllers\PenjualanKomputerController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PenjualanKomputerController::class, 'dashboard']);
Route::get('/list-penjualan', [PenjualanKomputerController::class, 'listPenjualan']);
Route::get('/detail-penjualan/{id}', [PenjualanKomputerController::class, 'detailPenjualan']);

